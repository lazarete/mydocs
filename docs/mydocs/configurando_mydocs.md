
# Estrutura atual de diretórios

```
.
├── docs
├── mydocs
│   └── configurando_mydocs.md
├── mkdocs.yml
├── README.md
└── requirements.txt

```
## Arquivos de configuração

### mkdocs.yml

```yml 
site_name: MyDocs 

nav:
  - Home: 'index.md'
  - Docker:
    - Instalação: 'docker/instalacao.md'
    - Configuração: 'docker/configuracao.md'
  - Python:
    - Instalação: './python/instalacao.md'
    - Funções: './python/funcoes.md'
  - Mydocs:
    - Configuração: './mydocs/configurando_mydocs.md'
markdown_extensions:
  - codehilite
  - pymdownx.arithmatex:
      generic: true  


theme:
  name: material
  #name: readthedocs
  palette: 

    # Palette toggle for light mode
    - scheme: default
      toggle:
        icon: material/brightness-7 
        name: Switch to dark mode

    # Palette toggle for dark mode
    - scheme: slate
      toggle:
        icon: material/brightness-4
        name: Switch to light mode
  theme:
    font:
     text: Ubuntu
     code: Ubuntu Mono

  icon:
    repo: fontawesome/brands/gitlab
    repo_url: https://gitlab.com/lazarete/mydocs.git
    repo_name: lazarete/mydocs

  features:
    - header.autohide
  
extra:
  generator: true
```  

### .gitlab-ci.yml

```yml
image: python:3.9

before_script:
  - pip install -r requirements.txt

pages:
  script:
    - pip install mkdocs
    - mkdocs build --site-dir public
  artifacts:
    paths:
      - public
  
  stage: deploy
  only:
    - main
```

### requirements.txt

```html
mkdocs==1.3.1
mkdocs-material==8.4.1
```
