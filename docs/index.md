# Bem vindo ao meu MkDocs

Visite a documentação oficial do MKdocs-material - [Material for MkDocs](https://squidfunk.github.io/mkdocs-material/).

## Comandos para iniciar seu projeto

* `mkdocs new [dir-name]` - Criando um novo projeto.
* `mkdocs serve` - Inicie o servidor de documentos.
* `mkdocs build` - Construa o site de documentação.
* `mkdocs -h` - Imprimir mensagem de ajuda e sair.

## Project layout

    mkdocs.yml    # O arquivo de configuração.
    docs/
        index.md  # A página inicial da documentação.
        ...       # Outras páginas de remarcação, imagens e outros arquivos.
